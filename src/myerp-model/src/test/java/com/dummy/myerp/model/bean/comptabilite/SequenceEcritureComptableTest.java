package com.dummy.myerp.model.bean.comptabilite;

import org.junit.Assert;
import org.junit.Test;

public class SequenceEcritureComptableTest {

    @Test
    public void setAnneeTest () {
        SequenceEcritureComptable sequenceEcritureComptable = new SequenceEcritureComptable();
        sequenceEcritureComptable.setAnnee(22);
        Assert.assertEquals(Integer.valueOf(22), sequenceEcritureComptable.getAnnee());
    }

    @Test
    public void toStringTest () {
        SequenceEcritureComptable sequenceEcritureComptable = new SequenceEcritureComptable(22, 20);
        Assert.assertEquals("SequenceEcritureComptable{annee=22, derniereValeur=20}", sequenceEcritureComptable.toString());
    }
}
