package com.dummy.myerp.model.bean.comptabilite;

import org.junit.Assert;
import org.junit.Test;

public class LigneEcritureComptableTest {

    @Test
    public void setLibelleTest () {
       LigneEcritureComptable ligneEcritureComptable = new LigneEcritureComptable();
        ligneEcritureComptable.setLibelle("Libelle");
        Assert.assertEquals("Libelle", ligneEcritureComptable.getLibelle());
    }


}
