package com.dummy.myerp.model.bean.comptabilite;

import org.junit.Assert;
import org.junit.Test;

public class CompteComptableTest {

    @Test
    public void setNumeroTest () {
        CompteComptable compteComptable = new CompteComptable();
        compteComptable.setNumero(1);
        Assert.assertEquals(Integer.valueOf(1), compteComptable.getNumero());
    }

    @Test
    public void setLibelleTest () {
        CompteComptable compteComptable = new CompteComptable();
        compteComptable.setLibelle("Libelle");
        Assert.assertEquals("Libelle", compteComptable.getLibelle());
    }

    @Test
    public void toStringTest () {
        CompteComptable compteComptable = new CompteComptable(1, "Libelle");
        Assert.assertEquals("CompteComptable{numero=1, libelle='Libelle'}", compteComptable.toString());
    }

}
