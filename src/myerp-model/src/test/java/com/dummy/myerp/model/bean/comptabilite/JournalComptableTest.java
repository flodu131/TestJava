package com.dummy.myerp.model.bean.comptabilite;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class JournalComptableTest {

    @Test
    public void setCodeTest () {
        JournalComptable journalComptable = new JournalComptable();
        journalComptable.setCode("AB");
        Assert.assertEquals("AB", journalComptable.getCode());
    }

    @Test
    public void setLibelleTest () {
        JournalComptable journalComptable = new JournalComptable();
        journalComptable.setLibelle("Libelle");
        Assert.assertEquals("Libelle", journalComptable.getLibelle());
    }

    @Test
    public void toStringTest () {
        JournalComptable journalComptable = new JournalComptable("AB", "Libelle");
        Assert.assertEquals("JournalComptable{code='AB', libelle='Libelle'}", journalComptable.toString());
    }

    @Test
            public void getJournalComptableAvecElementDansLaListe () {
        List<JournalComptable> journalComptables = new ArrayList<>();
        journalComptables.add(new JournalComptable("AB", "Libelle"));
        journalComptables.add(new JournalComptable("AC", "Libelle"));
        JournalComptable expected = new JournalComptable("AY", "Libelle");
        journalComptables.add(expected);
        Assert.assertEquals(expected,JournalComptable.getByCode(journalComptables, "AY"));

    }

    @Test
    public void getJournalComptableAvecElementDansLaListeEtElementVide () {
        List<JournalComptable> journalComptables = new ArrayList<>();
        journalComptables.add(new JournalComptable(null, "Libelle"));
        journalComptables.add(null);
        JournalComptable expected = new JournalComptable("AY", "Libelle");
        journalComptables.add(expected);
        Assert.assertEquals(expected,JournalComptable.getByCode(journalComptables, "AY"));

    }

    @Test
    public void getJournalComptableAvecElementpasDansLaListe () {
        List<JournalComptable> journalComptables = new ArrayList<>();
        journalComptables.add(new JournalComptable("AB", "Libelle"));
        journalComptables.add(new JournalComptable("AC", "Libelle"));
        journalComptables.add(new JournalComptable("AY", "Libelle"));
        Assert.assertNull(JournalComptable.getByCode(journalComptables, "BZ"));

    }

}
