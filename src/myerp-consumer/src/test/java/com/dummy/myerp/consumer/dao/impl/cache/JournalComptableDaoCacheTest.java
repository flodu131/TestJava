package com.dummy.myerp.consumer.dao.impl.cache;

import com.dummy.myerp.consumer.ConsumerHelper;
import com.dummy.myerp.consumer.dao.contrat.ComptabiliteDao;
import com.dummy.myerp.consumer.dao.contrat.DaoProxy;
import com.dummy.myerp.model.bean.comptabilite.CompteComptable;
import com.dummy.myerp.model.bean.comptabilite.JournalComptable;
import org.junit.*;
import org.mockito.MockedStatic;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.times;

public class JournalComptableDaoCacheTest {

    private static DaoProxy daoProxy = Mockito.mock(DaoProxy.class);
    private static MockedStatic<ConsumerHelper> consumerHelperMockedStatic;
    private JournalComptableDaoCache journalComptableDaoCache = new JournalComptableDaoCache();
    private ComptabiliteDao comptabiliteDao = Mockito.mock(ComptabiliteDao.class);

    @Before
    public void init() {
        Mockito.when(daoProxy.getComptabiliteDao()).thenReturn(comptabiliteDao);
    }

    @BeforeClass
    public static void initGlobal () {
        consumerHelperMockedStatic = Mockito.mockStatic(ConsumerHelper.class);
        consumerHelperMockedStatic.when(ConsumerHelper::getDaoProxy).thenReturn(daoProxy);
    }

    @AfterClass
    public static void closeGlobal () {
    consumerHelperMockedStatic.close();
    }

    @Test
    public void getByCode() {
        List<JournalComptable> journalComptable = new ArrayList<JournalComptable>();
      journalComptable.add(new JournalComptable("AC","Achat"));
      JournalComptable e = new JournalComptable("BC", "Vente");
      journalComptable.add(e);
        Mockito.when(comptabiliteDao.getListJournalComptable()).thenReturn(journalComptable);
        Assert.assertEquals(e, journalComptableDaoCache.getByCode("BC"));

    }

    @Test
    public void testListCallOneTime() {
        List<JournalComptable> journalComptable = new ArrayList<JournalComptable>();
        journalComptable.add(new JournalComptable("BC","Vente"));
        JournalComptable e = new JournalComptable("AC","Achat");
        journalComptable.add(e);
        Mockito.when(comptabiliteDao.getListJournalComptable()).thenReturn(journalComptable);
        Assert.assertEquals(e, journalComptableDaoCache.getByCode("AC"));
        Assert.assertEquals(e, journalComptableDaoCache.getByCode("AC"));
        Mockito.verify(comptabiliteDao,times(1)).getListJournalComptable();
    }

}