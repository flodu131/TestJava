package com.dummy.myerp.consumer.dao.impl.cache;

import com.dummy.myerp.consumer.ConsumerHelper;
import com.dummy.myerp.consumer.dao.contrat.ComptabiliteDao;
import com.dummy.myerp.consumer.dao.contrat.DaoProxy;
import com.dummy.myerp.model.bean.comptabilite.CompteComptable;
import org.junit.*;
import org.mockito.MockedStatic;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.times;


public class CompteComptableDaoCacheTest {

    private static DaoProxy daoProxy = Mockito.mock(DaoProxy.class);
    private static MockedStatic<ConsumerHelper> consumerHelperMockedStatic;
    private CompteComptableDaoCache compteComptableDaoCache = new CompteComptableDaoCache();
    private ComptabiliteDao comptabiliteDao = Mockito.mock(ComptabiliteDao.class);


    @Before
    public void init() {
        Mockito.when(daoProxy.getComptabiliteDao()).thenReturn(comptabiliteDao);
    }

    @BeforeClass
    public static void initGlobal () {
        consumerHelperMockedStatic = Mockito.mockStatic(ConsumerHelper.class);
        consumerHelperMockedStatic.when(ConsumerHelper::getDaoProxy).thenReturn(daoProxy);
    }

    @AfterClass
    public static void closeGlobal () {
        consumerHelperMockedStatic.close();
    }

    @Test
    public void testGetByNumero() {
        List<CompteComptable> compteComptable = new ArrayList<CompteComptable>();
        compteComptable.add(new CompteComptable(2));
        CompteComptable e = new CompteComptable(5);
        compteComptable.add(e);
        Mockito.when(comptabiliteDao.getListCompteComptable()).thenReturn(compteComptable);
        Assert.assertEquals(e, compteComptableDaoCache.getByNumero(5));
    }

    @Test
    public void testListCallOneTime() {
        List<CompteComptable> compteComptable = new ArrayList<CompteComptable>();
        compteComptable.add(new CompteComptable(2));
        CompteComptable e = new CompteComptable(5);
        compteComptable.add(e);
        Mockito.when(comptabiliteDao.getListCompteComptable()).thenReturn(compteComptable);
        Assert.assertEquals(e, compteComptableDaoCache.getByNumero(5));
        Assert.assertEquals(e, compteComptableDaoCache.getByNumero(5));
        Mockito.verify(comptabiliteDao,times(1)).getListCompteComptable();
    }

}