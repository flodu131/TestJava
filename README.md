# MyERP

## Organisation du répertoire

*   `doc` : documentation
*   `docker` : répertoire relatifs aux conteneurs _docker_ utiles pour le projet
    *   `dev` : environnement de développement
*   `src` : code source de l'application


## Environnement de développement

Les composants nécessaires lors du développement sont disponibles via des conteneurs _docker_.
L'environnement de développement est assemblé grâce à _docker-compose_
(cf docker/dev/docker-compose.yml).

Il comporte :

*   une base de données _PostgreSQL_ contenant un jeu de données de démo (`postgresql://127.0.0.1:9032/db_myerp`)

## Erreurs sur le projet

Dans la classe EcritureComptable le pattern a été modifié. En effet il vérifie désormais que le pattern est bien le bon avant le l'insérer. 
Dans la classe EcritureComptable le setScale(2) a été rajouter sur le retour des méthodes getTotalDebit et getTotalCredit. Cela permet d'avoir 2 chiffres après la virgule comme requis. 
Dans la classe EcritureComptable la méthode getTotalCredit utilisé getDebit() au lieu de getCredit(). 

### Lancement

    cd docker/dev
    docker-compose up


### Arrêt

    cd docker/dev
    docker-compose stop


### Remise à zero

    cd docker/dev
    docker-compose stop
    docker-compose rm -v
    docker-compose up
